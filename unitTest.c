
#include"symTable.h"

int main(){ 
    
    //creat
    printf("\n----creat Link ----\n");
    Link_t link = LinkTable_new();
   
    printf("\n---- insert nodes into Link----\n");
    int insert0 =  Link_put(link, "test0", "value");
    int insert1 =  Link_put(link, "test1", "vae");
    int insert2 =  Link_put(link, "test2", "value");
    int insert3 =  Link_put(link, "test3", "value");
    int insert4 =  Link_put(link, "test4", "value");

    //getLength
    printf("\n----getLength---\n%d\n",linkTable_getLength(link));
    
    //search key
    int result = Lint_contains(link,"test1");
    printf("\n----  search key result ----\n test1---%s\n",result == 1 ? "get it!" : "Not Found!");

    int result2 = Lint_contains(link,"test6");
    printf("test6--%s\n",result2 == 1 ? "get it!" : "Not Found!");

    //search value for key
    printf("\n----search key and return value ----\n key->%s, value->%s\n","test1",(char*)Link_get(link,"test1"));
    printf("\n----search key and return value ----\n key->%s, value->%s\n","test6",(char*)Link_get(link,"test6"));
   
    //replace
    Link_N node = Link_replace(link,"test0", "replaceSucess");
    printf("\n---- replace value if contains the key ----\n%s->%s\n",node->key,(char *)node->value);

    Link_N node2 = Link_replace(link,"test6", "vplace");
    printf("%s(test6)->%s\n",node2 ? node2->key : "Fail", node2 ? (char *)node2->value : "Not Found");

    //remove value and key
    Link_N node3 =  Link_remove(link,"test0");
      printf("remove node->test0 and return Node,%s\n",node3->key);

    Link_N node4 =  Link_remove(link,"test2");
    printf("remove node->test2 and return Node,%s\n",node4->key);

     Link_N node5 =  Link_remove(link,"test4");
    printf("remove node->test4 and return Node,%s\n",node5->key);
    //free 
    printf("\n----freelink---\n");
    LinkTable_free(link);

}


