
#include"symTable.h"

//creat link
Link_t  LinkTable_new(void)
{  
    Link_t link =  (Link_t)malloc(sizeof*(link));
    if (link == NULL)
        return NULL;
   link->head = NULL;
   link->tail = NULL;
   link->linkLength = 0;
   printf("creat sucess\n");
   return link;
}

//if !exit pcKey ,insert pcKey&pvValue ,else return 0
int Link_put(Link_t oLink, const char *pcKey, const void *pvValue)
{   
   
    if (oLink->head == NULL)
    {
        oLink->head = (Link_N)malloc(sizeof*((oLink->head))*100);
        oLink->head->key = (char *)malloc(sizeof(char) * 100);
        oLink->head->value = (char *)malloc(sizeof(char) * 100);
        strcpy(oLink->head->key, pcKey);
        strcpy(oLink->head->value, pvValue);
        oLink->head->next = oLink->tail;
        oLink->linkLength++;
        printf("insert complete%s\n",oLink->head->key);  
        oLink->tail = NULL;

    }
    else
    {   
        Link_N node = oLink->head;
        while(node->key && node->next)
        {

         if (strcmp(node->key, pcKey) == 0)
         {
            printf("have the same media\n");
            return 0;
         }
         node = node->next;
        }
        Link_N t = (Link_N)malloc(sizeof*(t));
        t->key = (char *)malloc(sizeof(char) * 30);
        t->value = (char *)malloc(sizeof(char) * 30);
        strcpy(t->key, pcKey);
        strcpy(t->value, pvValue);
        node->next = t;
        t->next = oLink->tail;
        oLink->linkLength++;
        printf("insert complete%s\n",t->key);  
    }
    return 0;
}

//if exit pcKey exchange pvValue ,else return NULL
void *Link_replace(Link_t oLink, const char *pcKey, const char *pvValue)
{
    Link_N node = oLink->head;
    if (node == NULL)
    {
        printf("HeadNode is NULL,please insert!\n");
        return NULL;
    }   
    while(node->key)
    {
        if (strcmp(node->key, pcKey) == 0)
        {
            strcpy(node->value, pvValue);
            return node;
        }
        if (node->next)
            node = node->next;
        else
            return NULL;
    }
   
    return NULL;

}

//search pcKey ,if get it return 1 ,else return 0
int Lint_contains(Link_t oLink,const char *pcKey)
{
    Link_N node = oLink->head;
    if (node == NULL)
    {
        printf(" HeadNode is NULL,please insert!\n");
        return 0;
    }   
    while(node->key)
    {
        if (strcmp(node->key, pcKey) == 0)
        {
            return 1;
        }
       if (node->next)
          node = node->next;
       else
          return 0; 
    }
    return 0;
}

//search pcKey ,if get it return pvValue else return NULL pointer
void *Link_get(Link_t oLink,const char *pcKey)
{
    Link_N node = oLink->head;
    if (node == NULL)
    {
        printf(" HeadNode is NULL,please insert!\n");
        return 0;
    }   
    while(node->key)
    {
        if (strcmp(node->key, pcKey) == 0)
        {
            return node->value;
        }
       if (node->next)
          node = node->next;
       else 
          return NULL;
    }
   
    return NULL;
}

//search pcKey ,remove the key&Value and return point else return NULL
void *Link_remove(Link_t oLink, const char *pcKey)
{
    Link_N node = oLink->head;
    Link_N before = NULL;
    if (node == NULL)
    {
        printf(" HeadNode is NULL,please insert!\n");
        return 0;
    }   
    while(node->key)
    {

        if (strcmp(node->key, pcKey) == 0)
        {
             if (before == NULL)
             {
                 before = node->next;
                 oLink->head = before;
             }             
             else 
                 before->next = node->next;
            free(node->key);
            free(node->value);
            free(node);
            oLink->linkLength--;
            printf("\nremove sucess!\n");
            return before;
        }
       if (node->next)
       {
          before = node;
          node = node->next;
       }
       else
          return NULL;
    }
   
    return NULL;
}

//number of linkLength
int linkTable_getLength(Link_t oLink)
{
    return oLink->linkLength;
}


//free link 
void LinkTable_free(Link_t oLink)
{
    if (oLink->head == NULL)
    {
        free(oLink);
        return;   
    }
    while (oLink->head)
    {
        Link_N node = oLink->head;
        oLink->head = oLink->head->next;
        oLink->linkLength--;
        free(node->key);
        node->key = NULL;
        free(node->value);
        node->value = NULL;      
        free(node);

        node = NULL;
    }
    free(oLink);
    printf("free success\n");
}





