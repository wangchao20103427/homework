#include<stdio.h>
#include <string.h>
#include <stdlib.h>
typedef struct LinkNode *Link_N;

struct LinkNode {
    char *key;
    void *value;
    Link_N next;
};

typedef struct Link *Link_t;

struct Link {
   Link_N head,tail;
    int linkLength;
};




//creat link
extern Link_t LinkTable_new(void);

//free link 
extern void LinkTable_free(Link_t oLink);

//number of linkLength
extern int linkTable_getLength(Link_t oLink);

//if !exit pcKey ,insert pcKey&pvValue ,else return 0
extern int Link_put(Link_t oLink, const char *pcKey, const void *pvValue);

//if exit pcKey exchange pvValue ,else return NULL
extern void *Link_replace(Link_t oLink, const char *pcKey, const char *pvValue);

//search pcKey ,if get it return 1 ,else return 0
extern int Lint_contains(Link_t oLink,const char *pcKey);

//search pcKey ,if get it return pvValue else return NULL pointer
extern void *Link_get(Link_t oLink,const char *pcKey);

//search pcKey ,remove the key&Value and return point else return NULL
extern void *Link_remove(Link_t oLink, const char *pcKey);







